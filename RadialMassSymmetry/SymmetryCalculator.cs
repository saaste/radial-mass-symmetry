﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RadialMassSymmetry
{
    /// <summary>
    /// Class for calculating radial mass symmetry
    /// </summary>
    public class SymmetryCalculator
    {

        /// <summary>
        /// Enum indicating if checked value is prefix or suffix
        /// </summary>
        private enum StringValueType
        {
            Prefix,
            Suffix
        }

        /// <summary>
        /// Calculate radial mass symmetry for a word
        /// </summary>
        /// <param name="word">Target word</param>
        /// <returns>String explaining the symmetry or "no symmetry"</returns>
        public static string Symmetry(string word)
        {
            string result = "no symmetry";

            // Empty strings and string shorter than 3 chars don't have symmetry
            if (!string.IsNullOrWhiteSpace(word) && word.Length >= 2)
            {
                // Skip first and last characters. Loop until symmetry is found
                // or the whole string has been checked
                for (int i = 1; i <= word.Length - 2; i++)
                {
                    // Get prefix and suffix from the string
                    string prefix = word.Substring(0, i);
                    string suffix = word.Substring(i + 1, word.Length - i - 1);

                    // Calculate masses for prefix and suffix
                    int prefixMass = CalculateRadialMass(prefix, StringValueType.Prefix);
                    int suffixMass = CalculateRadialMass(suffix, StringValueType.Suffix);

                    // If both masses are equal, symmetry has been found.
                    // Build the result string and exit the loop
                    if (prefixMass == suffixMass)
                    {
                        // Using string builder instead of string concatenation
                        StringBuilder resultBuilder = new StringBuilder();
                        resultBuilder.Append(prefix);
                        resultBuilder.Append(" ");
                        resultBuilder.Append(word[i]);
                        resultBuilder.Append(" ");
                        resultBuilder.Append(suffix);
                        resultBuilder.Append(" (");
                        resultBuilder.Append(prefixMass);
                        resultBuilder.Append(")");

                        result = resultBuilder.ToString();
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Check if the given string is valid.
        /// </summary>
        /// <param name="value">String to test</param>
        /// <returns>Boolean indicating if the string is valid for radial mass calculation</returns>
        public static bool IsValidString(string value)
        {
            return !string.IsNullOrWhiteSpace(value) && Regex.IsMatch(value, "^[a-zA-Z]+$");
        }

        /// <summary>
        /// Calculate radial mass for a string
        /// </summary>
        /// <param name="value">String value to test</param>
        /// <param name="partType">Is the string prefix or suffix</param>
        /// <returns>Calculated radial mass as an integer</returns>
        private static int CalculateRadialMass(string value, StringValueType partType)
        {
            int mass = 0;

            // If type is prefix, reverse the string because the last
            // letter should be one, second last should be two etc
            if (partType == StringValueType.Prefix)
            {
                char[] reversedPart = value.Reverse().ToArray();
                value = new string(reversedPart);
            }

            // Loop every character and add calculated value to the mass
            for (int i = 0; i < value.Length; i++)
            {
                int alphabetPosition = GetAlphabetPosition(value[i]);
                mass += (i + 1) * alphabetPosition;
            }

            return mass;
        }

        /// <summary>
        /// Get alphabet position. Letters from a to z are supperted.
        /// </summary>
        /// <param name="alphabet">Letter from a to z</param>
        /// <exception cref="ArgumentException">Throws when invalid alphabet was given</exception>
        /// <returns>Integer representing to number of alphabet. a = 1, b = 2 etc</returns>
        private static int GetAlphabetPosition(char alphabet)
        {
            // List of supported alphabets in order
            string alphabets = "abcdefghijklmnopqrstuvwxyz";

            // Convert character to lower case string
            string alphabetLower = alphabet.ToString().ToLower();

            // Get the index of an alphabet
            int index = alphabets.IndexOf(alphabetLower);

            // Alphabet not found. Throw an exception.
            if (index == -1)
            {
                throw new ArgumentException("Invalid alphabet", "alphabet");
            }

            // Add one to index so a will be 1, b will be 2 etc.
            return index + 1;
        }
    }
}
