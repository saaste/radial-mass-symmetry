﻿using System;

namespace RadialMassSymmetry
{
    /// <summary>
    /// Console app for calculating radial mass symmetry
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main app
        /// </summary>
        /// <param name="args">Command line arguments (not used)</param>
        static void Main(string[] args)
        {
            bool exitApp = false;

            // Run until letter q is entered
            while (!exitApp)
            {
                Console.Write("Enter a word or 'q' to exit: ");
                string word = Console.ReadLine();

                if (word.ToLower().Equals("q"))
                {
                    exitApp = true;
                }
                else if (!SymmetryCalculator.IsValidString(word))
                {
                    Console.WriteLine("String must be at least three characters long and contain only letters from A to Z!");
                    Console.WriteLine();
                }
                else
                {
                    string symmetry = SymmetryCalculator.Symmetry(word);
                    Console.Write("Radial mass symmetry: ");
                    Console.WriteLine(symmetry);
                    Console.WriteLine();
                }
            }
        }
    }
}
